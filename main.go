package main

import (
	"context"
	"errors"
	"flag"
	"fmt"
	"os"
	"strconv"
	"strings"
	"time"

	"github.com/prometheus/common/model"
	"github.com/prometheus/prometheus/pkg/timestamp"
	"github.com/prometheus/prometheus/template"
)

var (
	configFile = flag.String("c", "", "Path to configuration file")
	rootField  = flag.String("root", "main", "Root field of tests configuration")
)

func main() {
	err := realMain()
	if err != nil {
		_, _ = fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}
}

func realMain() error {
	flag.Parse()
	tests, err := LoadConfigFromFile(*configFile, *rootField)
	if err != nil {
		return err
	}
	var failed bool
	for testID, test := range tests {
		testName := strconv.Itoa(testID + 1)
		if test.Name != "" {
			testName = test.Name
		}
		for checkID, check := range test.Checks {
			tmplData := template.AlertTemplateData(check.Labels, map[string]string{}, "", 1.0)
			defs := []string{
				"{{$labels := .Labels}}",
				"{{$externalLabels := .ExternalLabels}}",
				"{{$externalURL := .ExternalURL}}",
				"{{$value := .Value}}",
			}
			exp := template.NewTemplateExpander(
				context.TODO(),
				strings.Join(append(defs, test.Template), ""),
				"text",
				tmplData,
				model.Time(timestamp.FromTime(time.Now())),
				nil,
				nil,
				nil,
			)
			text, err := exp.Expand()
			if err != nil {
				return err
			}
			checkName := strconv.Itoa(checkID + 1)
			if check.Name != "" {
				checkName = check.Name
			}
			if text != check.Result {
				failed = true
				fmt.Printf("%s/%s: fail (%s != %s)\n", testName, checkName, text, check.Result)
			} else {
				fmt.Printf("%s/%s: ok\n", testName, checkName)
			}
		}
	}
	if failed {
		return errors.New("failed")
	}
	return nil
}
