# prometheus-template-debug

Отладка шаблонизированных строк в алертах Prometheus.

## Применение

```shell
% ./prometheus-template-debug -c ./vhost-to-owner-team.config.yaml
rabbitmq-vhost-to-owner/vhost-finance: ok
rabbitmq-vhost-to-owner/vhost-communications-gateway: ok
rabbitmq-vhost-to-owner/vhost-foobar: ok
rabbitmq-vhost-to-owner/vhost-none: ok
```
