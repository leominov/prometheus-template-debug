package main

import (
	"fmt"
	"io/ioutil"

	"gopkg.in/yaml.v2"
)

type Config map[string][]*Test

type Test struct {
	Name     string   `yaml:"name"`
	Template string   `yaml:"template"`
	Checks   []*Check `yaml:"checks"`
}

type Check struct {
	Name   string            `yaml:"name"`
	Labels map[string]string `yaml:"labels"`
	Result string            `yaml:"result"`
}

func LoadConfigFromFile(filename, root string) ([]*Test, error) {
	b, err := ioutil.ReadFile(filename)
	if err != nil {
		return nil, err
	}
	config := make(map[string][]*Test)
	err = yaml.Unmarshal(b, &config)
	if err != nil {
		return nil, err
	}
	tests, ok := config[root]
	if !ok {
		return nil, fmt.Errorf("field %q not found", root)
	}
	return tests, nil
}
